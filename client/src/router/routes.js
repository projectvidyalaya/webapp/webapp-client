
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/servers', component: () => import('pages/servers.vue') },
      { path: '/dashboard', component: () => import('pages/dashboard.vue') },
      { path: '/alerts', component: () => import('pages/alerts.vue') },
      { path: '/login', component: () => import('pages/login.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
